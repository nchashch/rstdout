#![allow(non_snake_case)]
extern crate libc;
extern crate rmpv;
extern crate chrono;
extern crate fluent_bit_rust;

use fluent_bit_rust::output;

#[no_mangle]
unsafe fn FLBPluginRegister(def: *mut output::FLBPluginProxyDef) -> libc::c_int {
	  return output::FLBPluginRegister(def, "rstdout", "StdOut.rs");
}

// (fluentbit will call this)
// plugin (context) pointer to fluentbit context (state/ c code)
#[no_mangle]
unsafe fn FLBPluginInit(plugin: *mut output::FLBOutPlugin) -> libc::c_int {
	  // Example to retrieve an optional configuration parameter
	  let param = output::FLBPluginConfigKey(plugin, "param");
    println!("[flb-go] plugin parameter = '{:#?}'", param);
	  return output::FLB_OK;
}

#[no_mangle]
unsafe fn FLBPluginFlush(data: *const u8, length: libc::c_int, _tag: *const libc::c_char) -> libc::c_int {
    println!();
    let data_u8: &[u8] = std::slice::from_raw_parts(data, length as usize);
    let mut data_cursor = std::io::Cursor::new(data_u8);
    let record = rmpv::decode::read_value(&mut data_cursor).unwrap();
    let record = record.as_array().unwrap();
    // let ext = &record.as_slice()[0];
    let map = &record.as_slice()[1];
    let map = map.as_map().unwrap();
    let now: chrono::DateTime<chrono::Utc> = chrono::Utc::now();
    println!("{}", now.to_rfc2822());
    for (k, v) in map {
        println!("{}: {}", k, v);
    }
	  return output::FLB_OK;
}

#[no_mangle]
fn FLBPluginExit() -> libc::c_int {
	  return output::FLB_OK;
}
